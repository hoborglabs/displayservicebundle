<?php
// Define application environment
defined('APPLICATION_ENV')
    || define('APPLICATION_ENV', (getenv('APPLICATION_ENV') ? getenv('APPLICATION_ENV') : 'testing'));

defined('TEST_ROOT')
	|| define('TEST_ROOT', (getenv('TEST_ROOT') ?: __DIR__));

require_once __DIR__ . '/../vendors/autoload.php';

use Symfony\Component\ClassLoader\UniversalClassLoader;

$loader = new UniversalClassLoader();
$loader->registerNamespaces(array(
    'Hoborg' => __DIR__ . '/../src/',
));
$loader->register();
