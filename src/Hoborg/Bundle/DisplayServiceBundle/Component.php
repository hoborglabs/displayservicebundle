<?php
namespace Hoborg\Bundle\DisplayServiceBundle;

class Component implements IComponent {

	protected $name;

	public function generate($resource) {
		if (is_file($resource)) {
			return $this->generateFromFile($resource);
		}
	}

	public function edit($resource, $path) {
		if (is_file($resource)) {
			return $this->editFromFile($resource, $path);
		}
	}

	protected function generateFromFile($filePath) {
		if (!is_file($filePath)) {
			throw new Exception('Error in component generation.', 500);
			return;
		}

		// load XML
		$xmldoc = DOMDocument::load($filePath);
		$this->name = $xmldoc->documentElement->getAttribute('name');

		$proc = $this->getXSLTProcessor();

		return $proc->transformToXML($xmldoc);
	}

	protected function editFromFile($filePath, $path) {
		if (!is_file($filePath)) {
			throw new Exception('Error in component generation.', 500);
			return;
		}
		$configPath = preg_replace('/(.*)\.page/', '$1.cfg', $filePath);
		$config = json_decode(file_get_contents($configPath), true);

		$dom = new DOMDocument();
		$dom->load($filePath);
		$root = $dom->documentElement;
		$html = '<input type="hidden" name="path" value="/home'.$path.'" />';

		foreach ($root->childNodes as $component) {
			if ('component' === $component->nodeName) {
				$type = $component->getAttribute('name');
				if (isset($config[$type])) {
					$this->i++;
					$html .= $this->getEdit($component, "data[$type][".$this->i.']', $config[$type]);
				}
			}
		}

		return $html;
	}

	protected function getEdit(DOMElement $component, $type, array $config) {
		$html = '';
		$html .= 'component <strong>' . $component->getAttribute('name') .  '</strong><br />';
		$compName = $component->getAttribute('name');
		$this->i++;
		foreach ($component->childNodes as $data) {
			if ('data' === $data->nodeName) {
				foreach ($data->childNodes as $input) {
					if (in_array($input->nodeName, array_keys($config))) {
						$key = $input->nodeName;
						switch ($config[$key]) {
							case 'input':
								$html .= '<label style="display: block;width:200px; float: left;">'.$key.'</label><input size="50" type="text" name="'.$type.'['.$key.']" value="'.$input->nodeValue.'"/><br />';
								break;
							case 'tinyMCE':
								$html .= '<label style="display: block;width:200px; float: left;">'.$key.'</label><textarea name="'.$type.'['.$key.']" class="tinymce" cols="60" rows="20">'.$input->nodeValue.'</textarea>';
								break;
							default:
								break;
						}
					}
				}
			}
			if ('component' === $data->nodeName) {
				$cName = $data->getAttribute('name');
				if (isset($config[$cName])) {
					$html .= $this->getEdit($data, $type."[$cName][".$this->i.']', $config[$cName]);
				}
			}
		}
		return $html;
	}

	public function save($filePath, $data) {

		$configPath = preg_replace('/(.*)\.page/', '$1.cfg', $filePath);
		$config = json_decode(file_get_contents($configPath), true);

		$dom = new DOMDocument();
		$dom->load($filePath);
		$root = $dom->documentElement;

		foreach ($root->childNodes as $component) {
			if ('component' === $component->nodeName) {
				$type = $component->getAttribute('name');
				if (isset($config[$type])) {
					$this->saveComponent($component, array_pop($data[$type]), $config[$type]);
				}
			}
		}
		$fh = fopen($filePath, 'w');
		fwrite($fh, $dom->saveXML());
		fclose($fh);
	}

	protected function saveComponent(DOMElement $component, $componentData, $config) {
		foreach ($component->childNodes as $data) {
			if ('data' === $data->nodeName) {
				foreach ($data->childNodes as $input) {
					$key = $input->nodeName;
					if (isset($config[$key])) {
						$input->nodeValue = htmlentities($componentData[$key]);
					}
				}
			}
			if ('component' === $data->nodeName) {
				$cName = $data->getAttribute('name');
				if (isset($config[$cName])) {
					$this->saveComponent($data, array_shift($componentData[$cName]), $config[$cName]);
				}
			}
		}
	}

	protected function getXSLTProcessor($componentName = null) {
		if ($componentName == null) {
			$componentName = $this->name;
		}

		// get template
		$templatePath = Hoborg_DisplayService_Component_Configuration::getInstance()->getComponentTemplatePath($moduleName);
		$xsldoc = DOMDocument::load($templatePath);

		// transform component
		$proc = new XSLTProcessor();
		$proc->importStyleSheet($xsldoc);
		$proc->registerPhpFunctions();

		$proxy = Zend_Registry::isRegistered('hoborg.site.proxy.url') ?
		Zend_Registry::get('hoborg.site.proxy.url') : '';
		$proc->setParameter('', 'H_ROOT_PUBLIC', $proxy);

		return $proc;
	}
}

/**
 * Loads component
 */
function load_module($module_name, $data) {
	$edit_mode = Zend_Registry::isRegistered('hoborg.page.edit') ? Zend_Registry::get('hoborg.page.edit') : false;

	// get module name|file
	$module_name = $data[0]->getAttribute('name');
	$module_file = $data[0]->getAttribute('file');
	$module_xml = DOMDocument::loadXML('<data></data>');

	// load XML for component
	if ($module_file) {
		$component_xml = DOMDocument::load(Zend_Registry::get('hoborg.site.root') . Zend_Registry::get('hoborg.data.root') . DS.$module_file);
		$module_name = $component_xml->getElementsByTagName('component')->item(0)->getAttribute('name');
		$node = $module_xml->importNode($component_xml->getElementsByTagName('component')->item(0), true);
		$module_xml->documentElement->appendChild($node);
	}
	else {
		$node = $module_xml->importNode($data[0], true);
		$module_xml->documentElement->appendChild($node);
	}

	// now check if <data> object can be loaded
	$callouts = $module_xml->getElementsByTagName('callout');
	for ($i = 0; $i < $callouts->length; $i++) {
		$item = $callouts->item($i);
		$xml = '<root></root>';
		switch ($item->getAttribute('type')) {
			case 'php':
				$func = $callouts->item($i)->getAttribute('function') ? $callouts->item($i)->getAttribute('function') : 'main';

				// get params
				$params = array();
				$call_params = $item->getElementsByTagName('parameter');
				for ($j = 0; $j < $call_params->length; $j++) {

					array_push($params, $call_params->item($j)->nodeValue);
				}

				$xml = Hoborg_Callouts_PHP::call($item->getAttribute('file'), $func, $params);
				break;
			default:
				break;
		}

		// Now replace callout node with data returned by external script
		$replace_xml = DOMDocument::loadXML($xml);
		for ($j = 0; $j < $replace_xml->documentElement->childNodes->length; $j++) {
			$node = $module_xml->importNode($replace_xml->documentElement->childNodes->item($j), true);
			$item->parentNode->insertBefore($node, $item);
		}
		$item->parentNode->removeChild($item);
	}

	if (!$module_name && !$module_file) {
		return "<strong class=\"error\">module not loaded - you have to specify 'name' or 'file' attribute.</strong>";
	}

	$tempalte = Hoborg_DisplayService_Component_Configuration::getInstance()->getComponentTemplatePath($module_name);
	$local_xsldoc = DOMDocument::load($tempalte);
	$local_proc = new XSLTProcessor();
	$local_proc->importStyleSheet($local_xsldoc);
	$local_proc->registerPhpFunctions();

	$proxy = Zend_Registry::isRegistered('hoborg.site.proxy.url') ?
	Zend_Registry::get('hoborg.site.proxy.url') : '';
	$local_proc->setParameter('', 'H_ROOT_PUBLIC', $proxy);

	$output = $local_proc->transformToXML($module_xml);
	$componentId = $module_xml->getElementsByTagName('component')->item(0)->getAttribute('id');
	$componentParentId = $module_xml->getElementsByTagName('component')->item(0)->getAttribute('parent-id');
	if (true === $edit_mode) {
		return '<span class="HComponentEdit" component-name="' . $module_name . '" component-id="'. $componentId .'" component-parent-id="' . $componentParentId . '">'.$output.'</span>';
	}
	else {
		return $output;
	}
}
