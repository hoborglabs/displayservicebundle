<?php
namespace Hoborg\Bundle\DisplayServiceBundle\Tests\Component;

use Hoborg\Bundle\DisplayServiceBundle\Component\IProvider,
	Hoborg\Bundle\DisplayServiceBundle\Component\Provider;

class ProviderTest extends \PHPUnit_Framework_TestCase {

	/**
	 * @var Hoborg\Bundle\DisplayServiceBundle\Component\Provider
	 */
	protected $provider;

	public function setup() {
		$this->provider = $this->getProvider();
	}

	public function testConstructor() {
		$rootDir = TEST_ROOT . '/test.site.com';
		$provider = new Provider($rootDir);
		$this->assertInstanceOf('Hoborg\Bundle\DisplayServiceBundle\Component\Provider',
				$provider);
		$this->assertEquals(TEST_ROOT . '/test.site.com', $provider->getRootDir());
	}

	/**
	 * @dataProvider rootDirSpec
	 */
	public function testRootDir($input, $expected) {
		$provider = new Provider($input);
		$this->assertEquals($expected, $provider->getRootDir());
	}

	public function rootDirSpec() {
		$dirs = array();

		$dirs[] = array(TEST_ROOT . '/test.site.com', TEST_ROOT . '/test.site.com');
		$dirs[] = array(TEST_ROOT . '/../tests/test.site.com', TEST_ROOT . '/test.site.com');

		return $dirs;
	}

	public function testSitePrefix() {
		$this->assertEquals('site/', $this->provider->getSitePrefix());

		$this->provider->setSitePrefix('page/');
		$this->assertEquals('page/', $this->provider->getSitePrefix());
	}

	/**
	 * @expectedException Hoborg\Bundle\DisplayServiceBundle\Component\NotFoundError
	 */
	public function testgetByIdException() {
		$this->provider->getById('not-existing-page');
	}

	/**
	 * @return Hoborg\Bundle\DisplayServiceBundle\Component\Provider
	 */
	protected function getProvider($rootDir = null) {
		if (null == $rootDir) {
			$rootDir = TEST_ROOT . '/test.site.com';
		}
		$provider = new Provider($rootDir);
		return $provider;
	}

}
