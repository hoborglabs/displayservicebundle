<?php
namespace Hoborg\Bundle\DisplayServiceBundle\Tests\Service;

use Hoborg\Bundle\DisplayServiceBundle\Service\Display,
	Hoborg\Bundle\DisplayServiceBundle\Component\IProvider;

class DisplayTest extends \PHPUnit_Framework_TestCase {

	public function testConstructor() {
		$provider = $this->getProviderMock();
		$displayService = new Display($provider);
		$this->assertInstanceOf('Hoborg\Bundle\DisplayServiceBundle\Service\Display',
				$displayService);
	}

	protected function getProviderMock() {
		return $this->getMockBuilder('Hoborg\Bundle\DisplayServiceBundle\Component\Provider')
				->disableOriginalConstructor()
				->getMock();
	}

}
