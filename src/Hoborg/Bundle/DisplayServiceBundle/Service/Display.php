<?php
namespace Hoborg\Bundle\DisplayServiceBundle\Service;

use Hoborg\Bundle\DisplayServiceBundle\Component\ProviderEvent,
	Hoborg\Bundle\DisplayServiceBundle\Component\IProvider;
use Symfony\Component\EventDispatcher\EventDispatcher;

class Display implements IDisplay {

	/**
	 * @var Hoborg\Bundle\DisplayServiceBundle\Component\IProvider
	 */
	protected $componentProvider = null;

	/**
	 * @var Symfony\Component\EventDispatcher\EventDispatcher
	 */
	protected $eventDispatcher;

	protected $componentsConfiguration = null;

	public function __construct(IProvider $provider, EventDispatcher $dispatcher) {
		$this->componentProvider = $provider;
		$this->eventDispatcher = $dispatcher;

		// teleportation is not cool
		//\Hoborg\Bundle\DisplayServiceBundle\Component\Call::setConfiguration($configuration);
	}

	public function getRoot() {
		return $this->componentProvider->getRootDir();
	}

	public function setRoot($rootPath) {
		return $this->componentProvider->setRootDir($rootPath);
	}

	public function setBasePath($basePath) {
		return $this->componentProvider->setBasePath($basePath);
	}

	public function getBasePath() {
		return $this->componentProvider->getBasePath();
	}

	public function isEdit($isEdit = null) {
		if (null !== $isEdit) {
			return $this->componentProvider->isEdit($isEdit);
		}
		return $this->componentProvider->isEdit();
	}

	public function renderComponent($id) {
		// before load component
		$event = new ProviderEvent();
		$this->eventDispatcher->dispatch(ProviderEvent::BEFORE_LOAD, $event);

		$this->componentProvider->beforeLoad($id);

		// get component and all sub components
		$component = $this->componentProvider->getById($id);

		// after load
		$event = new ProviderEvent();
		$event->setComponent($component);

		// after load component
		$this->componentProvider->afterLoad($id);

		// after load
		$event = new ProviderEvent();
		$event->setComponent($component);

		$rendered = $component->render();

		$event = new ProviderEvent();
		$event->setOutput($rendered);

		return $event->getOutput();
	}
}
