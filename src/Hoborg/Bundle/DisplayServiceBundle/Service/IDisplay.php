<?php
namespace Hoborg\Bundle\DisplayServiceBundle\Service;

use Hoborg\Bundle\DisplayServiceBundle\Component\IProvider;
use Symfony\Component\EventDispatcher\EventDispatcher;

interface IDisplay {

	/**
	 * Returns rendered component.
	 *
	 * @param int|string $id
	 *
	 * @return string
	 */
	function renderComponent($id);
}