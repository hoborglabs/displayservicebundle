<?php
namespace Hoborg\Bundle\DisplayServiceBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller,
	Symfony\Component\HttpFoundation\Response;

class FrontController extends Controller {

	protected $response = null;
	protected $request = null;
	protected $displayServiceApi = null;

	/**
	 * Initialize objects shared accross all actions.
	 *
	 * @return void
	 */
	protected function init() {
		$this->displayServiceApi = $this->get('hoborg.displayService');
		$this->request = $this->getRequest();
	}

	public function renderAction($url) {
		$this->init();

		// get page path
		// get rendered page from hoborg.displayService
		$this->response = new Response();
		$this->response->setContent(
			$this->displayServiceApi->renderComponent($url)
		);

		return $this->response;
	}
}