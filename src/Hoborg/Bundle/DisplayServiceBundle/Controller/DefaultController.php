<?php
namespace Hoborg\Bundle\DisplayServiceBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller {

	public function displayAction() {

	}

	const STORE_ROOT = '/var/www/wojtek.oledzki.dev.local/';

	/**
	 * Hoborg logger object.
	 * @var Hoborg_Log_Logger
	 */
	private static $log;

	/**
	 * Relative path for data root folder (relative to site root).
	 * @var string
	 */
	protected $dataRoot = '';

	/**
	 * Full path for site (vhost).
	 * @var string
	 */
	protected $siteRoot = '';

	public function init() {
		// get logger object
		static::$log = Hoborg_Log::getLogger(__CLASS__);

		$format = (null == $this->_getParam('format')) ? 'html' : $this->_getParam('format');

		$this->view->setScriptPath(APPLICATION_PATH.DS.'DisplayService'.DS.'views'.DS.$format);
		$this->_helper->layout->setLayoutPath(APPLICATION_PATH.DS.'DisplayService'.DS.'views'.DS.$format);
		$this->_helper->layout->setLayout('page');

		// get page data root folder.
		if (defined('H_PAGE_DATA_ROOT')) {
			$this->dataRoot = H_PAGE_DATA_ROOT;
		}
		else {
			$cfg = Hoborg_Config_Loader::getModuleConfig('DisplayService');
			$this->dataRoot = Hoborg_Config_Helper::getValueByKey($cfg, 'page.data.root', '');
		}
		if (empty($this->dataRoot)) {
			throw new Exception('root page data folder not defined!', 101);
		}

		//get site root path
		if (defined('H_SITE_ROOTPATH')) {
			$this->siteRoot = H_SITE_ROOTPATH;
		}
		else {
			$cfg = Hoborg_Config_Loader::getModuleConfig('DisplayService');
			$this->siteRoot = Hoborg_Config_Helper::getValueByKey($cfg, 'site.root', '');
		}
		if (empty($this->siteRoot)) {
			throw new Exception('Site root path not defined!', 101);
		}
	}

	public function displayAction() {
		$request = $this->getRequest();
		$path = $request->getPathInfo();

		$DSApi = Hoborg_Rpc_Factory::getInstance('DisplayService')->getApi('SELF');
		$this->view->page = $DSApi->call('getPage', array($path, $this->siteRoot, $this->dataRoot));
	}

	public function editAction() {
		$cfg = Hoborg_Config_Loader::getModuleConfig('ContentService');

		$path = $this->_getParam('page', '');

		$project = Hoborg_ContentService_Project_Factory::create(dirname($this->dataRoot .DS. $path));
		$activeDir = $project->getActivePath($path);
		$root = $project->getOption(Hoborg_ContentService_iProject::OPTION_ROOT);

		$path = str_replace($root . DS. $activeDir, '', $path);
		//$relativePath = $project->getRelativePath();

		$fullPath = $this->dataRoot . DS . $root . DS. $activeDir . DS . $path;
		Zend_Registry::set('root', $root . DS . $activeDir);
		Zend_Registry::set('edit_mode', true);

		// create alternative path if current doesn't exist
		if (!is_file($fullPath)) {
			$fullPath =  $path . '/index.page';
		}

		// handle 404
		if (!is_file($fullPath)) {
			throw new Exception("Page not found! [$fullPath]'", 404);
		}

		$page = new Hoborg_Display_Page();

		// generate
		$this->view->page = $page->generate($fullPath);
	}

	public function getAction() {
		$file = $this->_getParam('file', '');

		$fullPath = $this->siteRoot .DS . $file;
		$ext = preg_replace('/.*\.([^.]*)/', '$1', $fullPath);
		$mimeType = 'text/plain';
		switch ($ext) {
			case 'css':
				$mimeType = 'text/css';
				break;
			default:
				$finfo = new finfo(FILEINFO_MIME_TYPE);
				$mimeType = $finfo->file($fullPath);
		}
		$this->getResponse()->setHeader('Content-Type', $mimeType);
		$this->view->file = $fullPath;
	}
}