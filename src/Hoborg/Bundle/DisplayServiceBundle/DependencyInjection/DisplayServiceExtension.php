<?php
namespace Hoborg\Bundle\DisplayServiceBundle\DependencyInjection;

use Symfony\Component\HttpKernel\DependencyInjection\Extension,
	Symfony\Component\DependencyInjection\ContainerBuilder,
	Symfony\Component\DependencyInjection\Loader\YamlFileLoader,
	Symfony\Component\Config\FileLocator;

class DisplayServiceExtension extends Extension {

	public function load(array $configs, ContainerBuilder $container) {

		$configuration = new Configuration();
		$config = $this->processConfiguration($configuration, $configs);

		$loader = new YamlFileLoader($container, new FileLocator(__DIR__ . '/../Resources/config'));
		$loader->load('services.yml');
	}

}