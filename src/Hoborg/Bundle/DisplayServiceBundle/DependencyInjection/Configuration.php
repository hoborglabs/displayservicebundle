<?php
namespace Hoborg\Bundle\DisplayServiceBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder,
	Symfony\Component\Config\Definition\ConfigurationInterface;

class Configuration implements ConfigurationInterface {

	public function getConfigTreeBuilder() {

		$treeBuilder = new TreeBuilder();
		$rootNode = $treeBuilder->root('hoborg_ds');

		$rootNode
			->end();

		return $treeBuilder;
	}
}