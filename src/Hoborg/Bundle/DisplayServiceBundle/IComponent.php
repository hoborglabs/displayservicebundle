<?php
namespace Hoborg\Bundle\DisplayServiceBundle;

interface IComponent {
	/**
	 * Component name
	 * @var string
	 */
	protected $name;

	/**
	 * Generates component.
	 *
	 * @param various $resource
	 * 		It can be path to .page file or file handler
	 */
	public function generate($resource);

	/**
	 * Generates component from .page file.
	 *
	 * @param String $filePath
	 * 		Full path to .page file
	 */
	protected function generateFromFile($filePath);

	/**
	 * Returns
	 * @param unknown_type $componentName
	 */
	protected function getXSLTProcessor($componentName = null);
}

?>