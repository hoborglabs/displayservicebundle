<?php
namespace Hoborg\Bundle\DisplayServiceBundle\Component;

interface IProvider {

	function getById($id);

	function getFromXml(\DOMElement $xml);

	function beforeLoad($id);

	function afterLoad($id);

	function afterRender();
}
