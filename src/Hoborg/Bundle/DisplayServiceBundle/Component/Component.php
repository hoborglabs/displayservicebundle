<?php
namespace Hoborg\Bundle\DisplayServiceBundle\Component;

use Symfony\Component\EventDispatcher\EventDispatcher;

/**
 * Default component class. This class needs to stay simple. Its main
 * and only purpose is to render component xml data.
 *
 * Everything else related to editing should be kept in component
 * providers.
 *
 */
class Component implements IComponent {

	/**
	 * Component XML.
	 * @var \DOMElement
	 */
	protected $xml = null;

	/**
	 * Component data document.
	 * This xml contains all elements needed for rendering this
	 * component.
	 *
	 * @var \DOMDocument
	 */
	protected $data = null;

	/**
	 * Components Provider.
	 *
	 * @var \Hoborg\Bundle\DisplayServiceBundle\Component\Provider
	 */
	protected $provider = null;

	/**
	 * @var string
	 */
	protected $name = null;

	/**
	 * @var string
	 */
	protected $id = null;

	/**
	 * Template name.
	 * @var string
	 */
	protected $template = 'default';

	/**
	 * Component root path.
	 * @var string
	 */
	protected $rootPath;

	/**
	 * Component configuration.
	 * @var array
	 */
	protected $conf = null;

	/**
	 * @var <Hoborg\Bundle\DisplayServiceBundle\Component\Component> array
	 */
	protected $subComponents = array();

	public function __construct(\DOMElement $componentXml, $componentRootFolder, IProvider $provider) {
		$this->xml = $componentXml;
		$this->data = new \DOMDocument();

		$this->name = $this->xml->getAttribute('name');
		$this->id = $this->xml->getAttribute('id');
		$this->setTemplate($this->xml->getAttribute('template'));
		$this->rootPath = realpath($componentRootFolder);

		$this->provider = $provider;
	}

	public function init() {
		$this->prepareData();
	}

	public function initData() {
		$this->initSubcomponentsData();
	}

	/**
	* @see Hoborg\Bundle\DisplayServiceBundle\Component\IComponent::prepareData()
	* @throws \Exception
	*/
	public function prepareData() {
		$data = $this->xml->getElementsByTagName('data');
		if (0 < $data->length) {
			$this->data->insertBefore($this->data->importNode($data->item(0), true));
		}

		$callouts = $this->xml->getElementsByTagName('callout');
		for ($i = 0; $i < $callouts->length; $i++) {
			$item = $callouts->item($i);
			$xml = '<root></root>';
			switch ($item->getAttribute('type')) {
				case 'php':
					$func = $callouts->item($i)->getAttribute('function') ?
							$callouts->item($i)->getAttribute('function') : 'main';

					// get params
					$params = array();
					$call_params = $item->getElementsByTagName('parameter');
					for ($j = 0; $j < $call_params->length; $j++) {
						$params[] = $call_params->item($j)->nodeValue;
					}

					$xml = Hoborg_Callouts_PHP::call($item->getAttribute('file'), $func, $params);
					break;
				default:
					break;
			}

			// Now replace callout node with data returned by external script
			$replace_xml = DOMDocument::loadXML($xml);
			for ($j = 0; $j < $replace_xml->documentElement->childNodes->length; $j++) {
				$node = $module_xml->importNode($replace_xml->documentElement->childNodes->item($j), true);
				$item->parentNode->insertBefore($node, $item);
			}
			$item->parentNode->removeChild($item);
		}
	}

	public function initSubcomponents(IProvider $provider) {
		foreach ($this->xml->childNodes as $component) {
			if ('component' === $component->nodeName) {
				$cmp = $provider->getFromXml($component);
				if ($cmp) {
					//$cmp->init();
					$this->subComponents[] = $cmp;
				}
			}
		}
		unset($component);

		// var_dump($this->name, count($this->subComponents));
	}

	public function registerEventListeners(EventDispatcher $eventDispatcher) {

	}

	private function initSubcomponentsData() {
		// foreach subcomponents ...
	}

	public function setTemplate($templateName) {
		if (empty($templateName)) {
			return;
		}
		$this->template = $templateName;
	}

	/**
	 * @see Hoborg\Bundle\DisplayServiceBundle\Component\IComponent::render()
	 * @throws \Exception
	 */
	public function render() {
		if (empty($this->xml)) {
			throw new \Exception('No component!', 500);
		}

		$subComponents = '';
		foreach ($this->subComponents as $component) {
			$subComponents .= $component->render();
		}

		$proc = $this->getXSLTProcessor();
		$proc->setParameter('', 'H_SUB_COMPONENTS', str_replace('\'', '&apos;', $subComponents));
		$proc->setParameter('', 'H_BASE_PATH', $this->provider->getBasePath());

		$xml = new \DOMDocument();
		$xml->loadXML('<component>' . $this->data->C14N() . '</component>');

		$output = $proc->transformToXML($xml);

		// @TODO: This should be moved to provider callback. Component
		// does not need to have that kind of knowledge.
		if ($this->provider->isEdit()) {
			return '<span class="HComponentEdit" data-mercury="full"'
					. " data-component-name=\"{$this->name}\""
					. " data-component-id=\"{$this->id}\""
					. " id=\"cmp-{$this->id}\""
					//.'" component-parent-id="' . $componentParentId
					. '>'.$output.'</span>';
		}
		return $output;
	}

	public function getData() {
		return $this->data;
	}

	public function getXml() {
		return $this->xml;
	}

	public function getConfiguration() {
		$cfgFile = $this->rootPath . '/component.cfg';

		$cfg = json_decode(file_get_contents($cfgFile), true);
		return $cfg;
	}

	public function getName() {
		return $this->name;
	}

	public function getId() {
		return $this->id;
	}

	protected function getXSLTProcessor($componentName = null) {
		if ($componentName == null) {
			$componentName = $this->name;
		}

		// get template
		$templatePath = "{$this->rootPath}/templates/{$this->template}.xsl";
		$xsldoc = new \DOMDocument();
		$xsldoc->load($templatePath);

		// transform component
		$proc = new \XSLTProcessor();
		$proc->importStyleSheet($xsldoc);
		$proc->registerPhpFunctions();

		// @TODO: site base path needs to be configurable.
		$proc->setParameter('', 'H_ROOT_PUBLIC', '');

		return $proc;
	}
}
