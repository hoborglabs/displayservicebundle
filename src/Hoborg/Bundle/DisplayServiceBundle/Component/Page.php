<?php
namespace Hoborg\Bundle\DisplayServiceBundle\Component;

/**
 * Hoborg.Display.Page
 * This class extends Component.
 *
 * @author wojtek oledzki
 *
 */
class Page extends Component {

    public static function createPage() {
        $page = new \DOMDocument();

        $c = $page->createElement('page');
        $c->setAttribute('name', 'page');

        $page->appendChild($c);

        return $page;
    }
}
