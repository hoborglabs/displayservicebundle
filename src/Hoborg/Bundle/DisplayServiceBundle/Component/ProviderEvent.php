<?php
namespace Hoborg\Bundle\DisplayServiceBundle\Component;

use Symfony\Component\EventDispatcher\Event;

class ProviderEvent extends Event {

	const BEFORE_LOAD = 'display_service.components.before_load';

	const POST_INIT = 'display_service.components.post_init';

	protected $component;

	protected $output = '';

	public function setComponent() {

	}

	public function getComponent() {

	}

	public function setOutput($output) {
		$this->output = $output;
	}

	public function getOutput() {
		return $this->output;
	}

}
