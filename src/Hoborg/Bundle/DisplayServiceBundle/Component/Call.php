<?php 
namespace Hoborg\Bundle\DisplayServiceBundle\Component;

class Call {

	protected static $config = '';

	public static function setConfiguration($config) {
		static::$config = $config;
	}

	public static function load_module($module_name, $data) {
		//Zend_Registry::isRegistered('hoborg.page.edit') ? Zend_Registry::get('hoborg.page.edit') : false;
		$edit_mode = false; 

		// get module name|file
		$module_name = $data[0]->getAttribute('name');
		$module_file = $data[0]->getAttribute('file');
		$module_xml = new \DOMDocument();
		$module_xml->loadXML('<data></data>');
		$siteRoot = static::$config->getRootDir();

		// load XML for component
		if ($module_file) {
			$component_xml = new \DOMDocument();
			$component_xml->load($siteRoot . DS. $module_file);
			$module_name = $component_xml->getElementsByTagName('component')->item(0)->getAttribute('name');
			$node = $module_xml->importNode($component_xml->getElementsByTagName('component')->item(0), true);
			$module_xml->documentElement->appendChild($node);
		}
		else {
			$node = $module_xml->importNode($data[0], true);
			$module_xml->documentElement->appendChild($node);
		}

		// now check if <data> object can be loaded
		$callouts = $module_xml->getElementsByTagName('callout');
		for ($i = 0; $i < $callouts->length; $i++) {
			$item = $callouts->item($i);
			$xml = '<root></root>';
			switch ($item->getAttribute('type')) {
				case 'php':
					$func = $callouts->item($i)->getAttribute('function') ? $callouts->item($i)->getAttribute('function') : 'main';

					// get params
					$params = array();
					$call_params = $item->getElementsByTagName('parameter');
					for ($j = 0; $j < $call_params->length; $j++) {

						array_push($params, $call_params->item($j)->nodeValue);
					}

					$xml = Hoborg_Callouts_PHP::call($item->getAttribute('file'), $func, $params);
					break;
				default:
					break;
			}

			// Now replace callout node with data returned by external script
			$replace_xml = DOMDocument::loadXML($xml);
			for ($j = 0; $j < $replace_xml->documentElement->childNodes->length; $j++) {
				$node = $module_xml->importNode($replace_xml->documentElement->childNodes->item($j), true);
				$item->parentNode->insertBefore($node, $item);
			}
			$item->parentNode->removeChild($item);
		}
	
		if (!$module_name && !$module_file) {
			return "<strong class=\"error\">module not loaded - you have to specify 'name' or 'file' attribute.</strong>";
		}
	
		$tempalte = static::$config->getComponentTemplatePath($module_name);

		$local_xsldoc = new \DOMDocument();
		$local_xsldoc->load($tempalte);

		$local_proc = new \XSLTProcessor();
		$local_proc->importStyleSheet($local_xsldoc);
		$local_proc->registerPhpFunctions();
	
		// $proxy = Zend_Registry::isRegistered('hoborg.site.proxy.url') ?
		//Zend_Registry::get('hoborg.site.proxy.url') : '';
		$local_proc->setParameter('', 'H_ROOT_PUBLIC', '');
	
		$output = $local_proc->transformToXML($module_xml);
		$componentId = $module_xml->getElementsByTagName('component')->item(0)->getAttribute('id');
		$componentParentId = $module_xml->getElementsByTagName('component')->item(0)->getAttribute('parent-id');
		if (true === $edit_mode) {
			return '<span class="HComponentEdit" component-name="' . $module_name . '" component-id="'. $componentId .'" component-parent-id="' . $componentParentId . '">'.$output.'</span>';
		}
		else {
			return $output;
		}
	}
}