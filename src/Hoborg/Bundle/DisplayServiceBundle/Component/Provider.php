<?php
namespace Hoborg\Bundle\DisplayServiceBundle\Component;

use Symfony\Component\EventDispatcher\EventDispatcher;

class Provider implements IProvider {

	const DS = DIRECTORY_SEPARATOR;

	/**
	 * Path to site folder relative to $rootDir
	 * @var string
	 */
	protected $sitePrefix = 'site/';

	protected $editMode = false;

	/**
	 * Path to site root.
	 * It's usualy your vhost root.
	 * @var string
	 */
	protected $rootDir = './';

	protected $basePath = '';

	/**
	 * @var Symfony\Component\EventDispatcher\EventDispatcher
	 */
	protected $eventDispatcher = null;

	/**
	 * Path to components configuration file.
	 * Relative to $this->rootDir.'/templates'
	 * @var string
	 */
	private $templatesConfiguration = 'configuration.xml';

	/**
	 * Configuration Xpath object.
	 * @var \DOMXPath
	 */
	private $templatesConfigurationXpath;

	public function __construct($rootDir, EventDispatcher $dispatcher) {
		$this->setRootDir($rootDir);
		$this->eventDispatcher = $dispatcher;
		$this->init();
	}

	public function init() {
		$this->templatesConfiguration = realpath($this->rootDir . static::DS . 'templates' .
				static::DS . $this->templatesConfiguration);

		if (empty($this->templatesConfiguration)) {
			throw new \Exception("No {$this->templatesConfiguration} file found in: " .
					$this->rootDir . static::DS . 'templates', 500);
		}

		// load configuration.
		$templatesConfig = new \DOMDocument();
		$templatesConfig->load($this->templatesConfiguration);
		$this->templatesConfigurationXpath = new \DOMXPath($templatesConfig);
	}

	public function setSitePrefix($sitePrefix) {
		$this->sitePrefix = $sitePrefix;

		return $this;
	}

	public function getSitePrefix() {
		return $this->sitePrefix;
	}

	public function getBasePath() {
		return $this->basePath;
	}

	public function setBasePath($basePath) {
		$this->basePath = $basePath;
	}

	public function isEdit($isEdit = null) {
		if (null !== $isEdit) {
			$this->editMode = true == $isEdit;
		}
		return $this->editMode;
	}

	public function getById($path) {

		// get path and optional component id
		$match = array();
		preg_match('#^(.*?(?:\.page)?)(?:/(\d+))?$#', $path, $match);
		$path = $match[1];
		$id = null;
		if (!empty($match[2])) {
			$id = $match[2];
		}

		$rootDir = $this->getRootDir() . '/' . $this->sitePrefix;
		$fullPath = $rootDir . '/' . $path;

		// create alternative path if current doesn't exist
		if (!is_file($fullPath)) {
			$fullPath = $rootDir . '/' . $path . '.page';
			if (!is_file($fullPath)) {
				$fullPath = $rootDir . '/' . $path . '/index.page';
			}
		}

		// handle 404
		if (!is_file($fullPath)) {
			throw new NotFoundError("Page not found [$fullPath]", 404);
		}

		$xml = new \DOMDocument();
		$xml->load($fullPath);

		// load subcomponent if subcomponent $id was provided.
		if ($id) {
			// xpath search
			$componentXpath = new \DOMXPath($xml);
			// component[id="$id"]
			$results = $componentXpath->query("//component[@id=\"{$id}\"]");
			if (1 != $results->length) {
				throw new \Exception("Component '{$id}' not found.", 500);
			}
			$componentXml = $results->item(0);
		} else {
			$componentXml = $xml->documentElement;
		}

		$component = $this->getFromXml($componentXml);

		return $component;
	}

	public function getFromXml(\DOMElement $xml) {
		$component = $this->createComponent($xml);
		return $component;
	}

	public function beforeLoad($id) {
		// no action
	}

	public function afterLoad($id) {
		// no action
	}

	public function afterRender() {
		// no action
	}

	public function getRootDir() {
		return $this->rootDir;
	}

	public function setRootDir($rootDir) {
		$this->rootDir = realpath($rootDir);
	}

	/**
	 *
	 * @param string $componentName
	 * @param string $template
	 */
	public function getComponentTemplatePath($componentName, $template = 'default') {
		$cmpFolder = $this->getComponentPath($componentName);

		if (empty($cmpFolder)) {
			throw new \Exception('component folder not found: ' . $this->rootDir .
					static::DS . $entries->item(0)->getAttribute('folder'), 500);
		}

		return $cmpFolder . static::DS . 'templates/' . $template . '.xsl';
	}

	/**
	 *
	 * @param String $componentName
	 */
	public function getComponentPath($componentName) {
		$entries = $this->templatesConfigurationXpath->query('//templates/component[@name="' . $componentName . '"]');
		if (1 != $entries->length) {
			throw new \Exception("Component '$componentName' not found. Please check '".
					$this->templatesConfiguration ."'", 500);
		}

		return realpath($this->rootDir . static::DS . 'templates' . static::DS . $entries->item(0)->getAttribute('folder'));
	}

	/**
	 * Returns page template xsl path.
	 *
	 * @param string $componentName
	 *
	 * @return string
	 */
	public function getLayoutTemplatePath($componentName, $template = 'default') {
		$entries = $this->templatesConfigurationXpath->query('//templates/page[@name="' . $componentName . '"]');
		if (1 != $entries->length) {
			throw new \Exception("Template '$componentName' not found. Please check {$this->templatesConfiguration}", 500);
		}

		$cmpFolder = realpath($this->rootDir . '/templates/' . $entries->item(0)->getAttribute('folder'));

		if (empty($cmpFolder)) {
			throw new \Exception('component folder not found: ' . $this->rootDir . static::DS . $entries->item(0)->getAttribute('folder'), 500);
		}

		return $cmpFolder . static::DS . 'templates/'.$template.'.xsl';
	}

	public function getLayouts() {
		$entries = $this->templatesConfigurationXpath->query('//templates/page');
		Hoborg_Log::debug(__METHOD__ . ' page nodes count: ' . $entries->length);
		$layouts = array();

		for ($i = 0; $i < $entries->length; $i++) {
			$layouts[$entries->item($i)->getAttribute('name')] = $entries->item($i)->getAttribute('name');
		}

		return $layouts;
	}

	public function getComponentConfiguration($componentName) {
		$cmpPath = $this->getComponentPath($componentName);

		$configuration = json_decode(file_get_contents($cmpPath . '/component.cfg'), true);
		// 		foreach ($configuration['data'] as $key => $item) {
		// 			$configuration['data'][$item['path']] = $item;
		// 			unset($configuration['data'][$key]);
		// 		}

		return $configuration;
	}

	protected function createComponent(\DOMElement $xml) {
		$componentFile = $xml->getAttribute('file');
		if ($componentFile) {
			$componentXml = new \DOMDocument();
			$componentXml->load($this->getRootDir() . '/' . $this->sitePrefix . static::DS. $componentFile);
			$module_name = $componentXml->getElementsByTagName('component')->item(0)->getAttribute('name');
			$xml = $componentXml->getElementsByTagName('component')->item(0);
		}

		$componentName = $xml->getAttribute('name');
		if (empty($componentName)) {
			throw Exception('Component is missing `name` and `file`');
		}

		$componentConfiguration = $this->getComponentConfiguration($componentName);
		$componentFolder = $this->getComponentPath($componentName);
		$componentClass = empty($componentConfiguration['class']) ?
				'\\Hoborg\\Bundle\\DisplayServiceBundle\\Component\\Component' : $componentConfiguration['class'];

		if (is_readable($componentFolder . '/Component.php')) {
			include_once $componentFolder . '/Component.php';
		}

		$component = new $componentClass($xml, $componentFolder, $this);

		$component->init();
		$component->initSubcomponents($this);
		if ($this->eventDispatcher) {
			$component->registerEventListeners($this->eventDispatcher);
		}
		//$component->prepareData();

		return $component;
	}

	protected function getFullPath($path) {
		$rootDir = $this->getRootDir() . '/' . $this->sitePrefix;
		$fullPath = $rootDir . '/' . $path;

		// create alternative path if current doesn't exist
		if (!is_file($fullPath)) {
			$fullPath = $rootDir . '/' . $path . '.page';
			if (!is_file($fullPath)) {
				$fullPath = $rootDir . '/' . $path . '/index.page';
			}
		}

		return $fullPath;
	}
}
