<?php
namespace Hoborg\Bundle\DisplayServiceBundle\Component;

use Symfony\Component\EventDispatcher\EventDispatcher;

interface IComponent {

	function init();

	function prepareData();

	function getData();

	function getConfiguration();

	/**
	 * Generates component.
	 *
	 * @param various $resource
	 * 		It can be path to .page file or file handler
	 */
	function render();

	function setTemplate($templateName);

	function initSubcomponents(IProvider $componentProvider);

	function registerEventListeners(EventDispatcher $eventDispatcher);

}
