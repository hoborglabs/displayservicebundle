<?php
namespace Hoborg\Bundle\DisplayServiceBundle\Component\Data;

class Callout {

	public function callPhp($file, $func, $params = null) {
		$root = ''; //Zend_Registry::get('hoborg.data.root');
		$path = ''; //Zend_Registry::get('hoborg.site.root') . $root . DS . $file;

		if (!is_file($path)) {
			return '<xml><error><span>File ('.$path.') not found!</span></error></xml>';
		}

		// prepare parameters
		$call_params = array("'".quotemeta($func)."'");
		foreach ($params as &$param) {
			array_push($call_params, "'".quotemeta($param)."'");
		}
		$call_params = join(',', $call_params);

		// evaluate the callout
		$data = eval("require_once '".$path."'; return call_user_func($call_params);");

		return $data;
	}
}