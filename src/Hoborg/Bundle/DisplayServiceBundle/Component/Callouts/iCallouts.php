<?php
interface Hoborg_Callouts_iCallouts {

    /**
     * Calls external script. This function is expected to return valid XML.
     *
     * @param string $file
     * @param string $func
     * @param array $params
     * @return string
     */
    public function call($file, $func, $params = null);
}
