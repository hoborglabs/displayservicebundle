<?php
namespace Hoborg\Bundle\DisplayServiceBundle\Component;

class Configuration {

	private $templatesConfiguration = 'configuration.xml';
	private $templatesConfigurationXpath;
	private static $singleton = null;

	protected $rootDir = '';

	/**
	 * Constructor
	 */
	public function __construct($rootDir) {
		$this->rootDir = $rootDir;
		$this->templatesConfiguration = realpath($rootDir . DS . 'templates' . DS . 'configuration.xml');
		if (empty($this->templatesConfiguration)) {
			throw new \Exception('No configuration.xml file found in: ' . $rootDir . DS . 'templates', 500);
		}

		$templatesConfig = new \DOMDocument();
		$templatesConfig->load($this->templatesConfiguration);

		$this->templatesConfigurationXpath = new \DOMXPath($templatesConfig);
		//Hoborg_Log::debug(__METHOD__ . ' configuration loaded from ' . $this->templatesConfiguration);
	}

	public function getRootDir() {
		return $this->rootDir;
	}

	/**
	 *
	 * @param String $componentName
	 */
	public function getComponentTemplatePath($componentName, $template = 'default') {
		$entries = $this->templatesConfigurationXpath->query('//templates/component[@name="' . $componentName . '"]');
		if (1 != $entries->length) {
			throw new \Exception("Template '$componentName' not found. Please check {$this->templatesConfiguration}", 500);
			return '';
		}

		$cmpFolder = realpath($this->rootDir . '/templates/' . $entries->item(0)->getAttribute('folder'));

		if (empty($cmpFolder)) {
			throw new \Exception('component folder not found: ' . $this->rootDir . DS . $entries->item(0)->getAttribute('folder'), 500);
		}

		return $cmpFolder . DS . 'templates/'.$template.'.xsl';
	}

	/**
	 *
	 * @param String $componentName
	 */
	public function getComponentPath($componentName) {
		$entries = $this->templatesConfigurationXpath->query('//templates/component[@name="' . $componentName . '"]');
		if (1 != $entries->length) {
			throw new \Exception("Component '$componentName' not found. Please check '".
					$this->templatesConfiguration ."'", 500);
			return '';
		}

		return $this->rootDir . DS . 'templates' . DS . $entries->item(0)->getAttribute('folder');
	}

	/**
	 * Returns page template xsl path.
	 *
	 * @param string $componentName
	 *
	 * @return string
	 */
	public function getLayoutTemplatePath($componentName, $template = 'default') {
		$entries = $this->templatesConfigurationXpath->query('//templates/page[@name="' . $componentName . '"]');
		if (1 != $entries->length) {
			throw new \Exception("Template '$componentName' not found. Please check {$this->templatesConfiguration}", 500);
		}

		$cmpFolder = realpath($this->rootDir . '/templates/' . $entries->item(0)->getAttribute('folder'));

		if (empty($cmpFolder)) {
			throw new \Exception('component folder not found: ' . $this->rootDir . DS . $entries->item(0)->getAttribute('folder'), 500);
		}

		return $cmpFolder . DS . 'templates/'.$template.'.xsl';
	}

	public function getLayouts() {
		$entries = $this->templatesConfigurationXpath->query('//templates/page');
		Hoborg_Log::debug(__METHOD__ . ' page nodes count: ' . $entries->length);
		$layouts = array();

		for ($i = 0; $i < $entries->length; $i++) {
			$layouts[$entries->item($i)->getAttribute('name')] = $entries->item($i)->getAttribute('name');
		}

		return $layouts;
	}

	public function getComponentConfiguration($componentName) {
		$configuration = array();
		$cmpPath = $this->getComponentPath($componentName);

		$configuration = json_decode(file_get_contents($cmpPath . '/component.cfg'), true);
// 		foreach ($configuration['data'] as $key => $item) {
// 			$configuration['data'][$item['path']] = $item;
// 			unset($configuration['data'][$key]);
// 		}

		return $configuration;
	}
}