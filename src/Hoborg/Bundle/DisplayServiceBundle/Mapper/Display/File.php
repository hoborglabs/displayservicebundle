<?php
namespace Hoborg\Bundle\DisplayServiceBundle\Mapper\Display;

class File implements \Hoborg\Bundle\DisplayServiceBundle\Component\IProvider {

	protected $configuration;
	protected $sitePrefix = 'site/';

	public function getById($path) {
		$rootDir = $this->configuration->getRootDir() . '/' . $this->sitePrefix;
		$fullPath = $rootDir . '/' . $path;

		// create alternative path if current doesn't exist
		if (!is_file($fullPath)) {
			$fullPath = $rootDir . '/' . $path . '.page';
			if (!is_file($fullPath)) {
				$fullPath = $rootDir . '/' . $path . '/index.page';
			}
		}

		// handle 404
		if (!is_file($fullPath)) {
			throw new \Exception("Page not found [$fullPath]", 404);
		}

		$xml = new \DOMDocument();
		$xml->load($fullPath);
		$component = new \Hoborg\Bundle\DisplayServiceBundle\Component\Page($xml, $this->configuration);
		return $component;
	}

	public function getFromXml($xml) {

	}

	public function setConfiguration($configuration) {
		$this->configuration = $configuration;
	}

	public function beforeLoad($id) {
		//
	}

	public function afterLoad($id) {
		//
	}
}
