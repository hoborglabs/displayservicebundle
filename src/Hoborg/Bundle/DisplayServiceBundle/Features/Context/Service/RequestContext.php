<?php
namespace Hoborg\Bundle\DisplayServiceBundle\Features\Context\Service;

use Behat\BehatBundle\Context\BehatContext,
	Behat\BehatBundle\Context\MinkContext,
	Behat\Behat\Context\ClosuredContextInterface,
	Behat\Behat\Context\TranslatedContextInterface,
	Behat\Behat\Exception\PendingException,
	Behat\Gherkin\Node\PyStringNode,
	Behat\Gherkin\Node\TableNode;

class RequestContext extends BehatContext {

	/**
	 * @When /^I request a page "([^"]*)"$/
	 */
	public function iRequestAPage($path) {
		$container = $this->getContainer();
		$page = $container->get('hoborg.displayservice')->getPage($path);
	}
}
