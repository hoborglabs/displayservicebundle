<?php

namespace Hoborg\Bundle\DisplayServiceBundle\Features\Context;

use Hoborg\Bundle\CommonsBundle\Features\Context\Api\ResponseContext,
	Behat\BehatBundle\Context\BehatContext,
	Behat\BehatBundle\Context\MinkContext,
	Behat\Behat\Context\ClosuredContextInterface,
	Behat\Behat\Context\TranslatedContextInterface,
	Behat\Behat\Exception\PendingException,
	Behat\Gherkin\Node\PyStringNode,
	Behat\Gherkin\Node\TableNode,
	Behat\Behat\Event\SuiteEvent;

//
// Require 3rd-party libraries here:
//
//   require_once 'PHPUnit/Autoload.php';
//   require_once 'PHPUnit/Framework/Assert/Functions.php';
//

/**
 * Feature context.
 */
class FeatureContext extends BehatContext {

	public function __construct($parameters) {
		parent::__construct($parameters);

		$this->useContext('service_request', new Service\RequestContext($parameters));
		$this->useContext('service_response', new Service\ResponseContext($parameters));
	}

	/** @BeforeSuite */
	public static function setup(SuiteEvent $event) {
	}

	/** @AfterSuite */
	public static function teardown(SuiteEvent $event) {
	}

}
